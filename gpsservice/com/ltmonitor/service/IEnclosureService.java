package com.ltmonitor.service;

import java.util.List;

import com.ltmonitor.dao.IBaseDao;
import com.ltmonitor.entity.Enclosure;
import com.ltmonitor.entity.LineBufferPoint;
import com.ltmonitor.entity.LineSegment;

public interface IEnclosureService extends IBaseDao{

	void saveRoute(Enclosure ec, List<LineSegment> segments);

	List<LineSegment> getLineSegments(int enclosureId);
	

}