package com.ltmonitor.service;

import java.io.Serializable;
import java.util.List;

import com.ltmonitor.entity.BasicData;


public interface IBasicDataService {

	public abstract List getBaseDataByParentID(Serializable parentID);


	public abstract BasicData getBasicDataByCode(String code, String parentCode);

	public abstract BasicData getBasicDataByName(String name, String parentCode);

	
	public abstract List<BasicData> getBasicDataByParentCode(String parentCode);

	
	public abstract BasicData getBasicDataByID(Serializable ID);

}