package com.ltmonitor.service.impl;

import java.util.HashMap;

import com.ltmonitor.dao.impl.DaoHibernateImpl;
import com.ltmonitor.entity.Department;
import com.ltmonitor.entity.VehicleData;
import com.ltmonitor.service.IDepartmentService;

public class DepartmentService extends DaoHibernateImpl implements
		IDepartmentService {

	/**
	 * 部门基本信息缓存
	 */
	public static HashMap<Integer, Department> depMap = new HashMap<Integer, Department>();

	public Department getDepartment(int depId) {
		if (depMap.containsKey(depId)) {
			return depMap.get(depId);
		}
		Department d =  (Department) load(Department.class, depId);
		depMap.put(depId, d);
		return d;
	}
	
	public void saveDepartment(Department dep){
		this.saveOrUpdate(dep);
		depMap.put(dep.getEntityId(), dep);
	}

}
