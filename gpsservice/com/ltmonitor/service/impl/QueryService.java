package com.ltmonitor.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ltmonitor.dao.impl.DaoIbatisImpl;
import com.ltmonitor.entity.VehicleData;
import com.ltmonitor.service.IQueryService;

public class QueryService implements IQueryService {
	
	private DaoIbatisImpl  queryDao;
	
	
	@Override
	public List<VehicleData> getVehicleListByUserId(int userId)
	{		
		List<VehicleData> result = new ArrayList<VehicleData>();
		String queryId = "selectVehicles";
		
		Map params = new HashMap();
		params.put("userId", userId);
		try
		{
		getQueryDao().queryForList(queryId, params );
		}catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		return result;
	}


	public DaoIbatisImpl getQueryDao() {
		return queryDao;
	}


	public void setQueryDao(DaoIbatisImpl queryDao) {
		this.queryDao = queryDao;
	}

}
