package com.ltmonitor.service;

import com.ltmonitor.entity.JT809Command;
import com.ltmonitor.entity.TerminalCommand;

public interface ITerminalService {

	public abstract void SendCommand(TerminalCommand tc);

	void SendPlatformCommand(JT809Command tc);

}