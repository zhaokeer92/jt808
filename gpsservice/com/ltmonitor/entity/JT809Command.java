package com.ltmonitor.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="jt809Command")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class JT809Command extends TenantEntity{

	public static final String STATUS_NEW = "New";
	public static final String STATUS_PROCESSING = "Processing";
	public static final String STATUS_INVALID = "Invalid";
	public static final String STATUS_SUCCESS = "Success";
	public static final String STATUS_FAILED = "Failed";
	public static final String STATUS_Disconnected = "disconnected";
	
	public static final String STATUS_RECEIVED = "Received";
	
	public static final String DOWN_NOTIFY = "down_notify";
	public static final String DOWN_ACK = "down_ack";
	public static final String UP_JT808 = "up_jt808";
	public static final String UP_USER = "up_user";
	
	public JT809Command()
	{
		status = STATUS_NEW;
		setCreateDate(new Date());
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cmdId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	
	//终端ID号
    private String simNo;
    //车牌号
    private String plateNo;
    //车牌颜色
    private byte plateColor;

    //子命令类别
    private int subCmd;
    //命令
    private int cmd;

    private String descr;

    //数据，多个参数的时候，使用;隔开
    private String cmdData;

    //命令状态
    private String status;
    //命令下发的流水号
    private int SN;
    
    private int userId;
    //区别命令的来源，下发的动作等
    private String source;
    
    private Date updateDate;

	public int getSN() {
		return SN;
	}
	public void setSN(int sN) {
		SN = sN;
	}
	public String getSimNo() {
		return simNo;
	}
	public void setSimNo(String simNo) {
		this.simNo = simNo;
	}
	public String getPlateNo() {
		return plateNo;
	}
	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}
	public byte getPlateColor() {
		return plateColor;
	}
	public void setPlateColor(byte plateColor) {
		this.plateColor = plateColor;
	}
	public int getSubCmd() {
		return subCmd;
	}
	public void setSubCmd(int subCmd) {
		this.subCmd = subCmd;
	}
	public int getCmd() {
		return cmd;
	}
	public void setCmd(int cmd) {
		this.cmd = cmd;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getCmdData() {
		return cmdData;
	}
	public void setCmdData(String cmdData) {
		this.cmdData = cmdData;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}


}
