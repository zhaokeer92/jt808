package com.ltmonitor.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SystemConfig")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
@org.hibernate.annotations.Proxy(lazy = false)
public class SystemConfig extends TenantEntity implements Serializable {
	private static final long serialVersionUID = -7442032908999437416L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int entityId;

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int value) {
		entityId = value;
	}

	@Column(name = "update_latlon_interval")
	private int updateLatlonInterval;

	@Column(name = "gpsdata_delay_time")
	private Integer gpsdataDelayTime;

	@Column(name = "alaram_wave_flag")
	private Integer alaramWaveFlag;

	@Column(name = "system_title")
	private String systemTitle;

	@Column(name = "google_url")
	private String googleUrl;

	@Column(name = "google_version")
	private String googleVersion;

	@Column(name = "google_key1")
	private String googleKey1;

	@Column(name = "google_key2")
	private String googleKey2;

	@Column(name = "mapabc_key1")
	private String mapabcKey1;

	@Column(name = "mapabc_key2")
	private String mapabcKey2;

	@Column(name = "authentication_code")
	private String authenticationCode;

	@Column(name = "smart_key")
	private String smartKey;

	@Column(name = "initLat")
	private double initLat;

	@Column(name = "initLng")
	private double initLng;

	@Column(name = "initZoomLevel")
	private Integer initZoomLevel;

	@Column(name = "mapType")
	private Integer mapType;

	@Column(name = "superior_gov_status")
	private Integer superiorGovStatus;

	@Column(name = "start_govment_connect")
	private Integer startGovmentConnect;

	@Column(name = "center_server_status")
	private Integer centerServerStatus;

	@Column(name = "govment_center_id")
	private Long govmenterCenterId;

	@Column(name = "government_ip")
	private String governmentIp;

	@Column(name = "goverment_port")
	private Integer govermentPort;

	@Column(name = "government_account")
	private String governmentAccount;

	@Column(name = "government_password")
	private String governmentPassword;

	@Column(name = "gov_sub_connect_ip")
	private String govSubConnectIp;

	@Column(name = "gov_sub_connect_port")
	private Integer govSubConnectPort;

	@Column(name = "gov_sub_connect_status")
	private Integer govSubConnectStatus;

	@Column(name = "govment_m1")
	private Long govmentM1;

	@Column(name = "govment_ia1")
	private Long govmentIa1;

	@Column(name = "govment_ic1")
	private Long govmentIc1;

	@Column(name = "govment_encrypt_flag")
	private Boolean govmentEncryptFlag;

	@Column(name = "logo_flag")
	private Integer logoFlag;

	@Column(name = "driverschool_flag")
	private Integer driverschoolFlag;

	@Column(name = "taxi_flag")
	private Integer taxiFlag;

	@Column(name = "driver_term_type")
	private Integer driverTermType = Integer.valueOf(30);

	@Column(name = "js_debug_flag")
	private Integer jsDebugFlag;

	@Column(name = "gov_connect_info")
	private Integer govConnectInfo;

	@Column(name = "pop_config")
	private Integer popConfig;

	@Column(name = "accessPoint_name")
	private String accessPointName;

	@Column(name = "auth_username")
	private String authUsername;

	@Column(name = "auth_password")
	private String authPassword;

	@Column(name = "urgency_server_ip")
	private String urgencyServerIp;

	@Column(name = "urgency_tcp_port")
	private Integer urgencyTcpPort;

	@Column(name = "urgency_udp_port")
	private Integer urgencyUdpPort;

	@Column(name = "post_query_interval")
	private Integer postQueryInteval;

	@Column(name = "alarm_redo_time")
	private Integer alarmRedoTime;

	@Column(name = "alarm_redo_end_time")
	private Integer alarmRedoEndTime;

	@Column(name = "alarm_supervisor")
	private String alarmSupervisor;

	@Column(name = "alarm_supervisor_mail")
	private String alarmSupervisorMail;

	@Column(name = "alarm_supervisor_tel")
	private String alarmSupervisorTel;

	@Column(name = "cross_region_flag")
	private Integer crossReginFlag = Integer.valueOf(0);

	@Column(name = "polling_online_compute_time")
	private Integer pollingOnLineCommputeTime = Integer.valueOf(5);

	@Column(name = "auto_post_query_cmd_inteval")
	private Integer autoPostQueryCmdInteval;

	@Column(name = "yz_interface_ip")
	private String yzInterfaceIp;

	@Column(name = "yz_interface_port")
	private Long yzInterfacePort;

	@Column(name = "yz_user_id")
	private String yzUserId;

	@Column(name = "yz_password")
	private String yzPassword;

	@Column(name = "car_data_down_time")
	private Integer carDataDownTime;

	@Column(name = "car_control_password")
	private String carControlPassword;

	@Column(name = "system_flag")
	private Integer systemFlag;

	@Column(name = "system_mode")
	private Integer systemMode;

	@Column(name = "g3_media_ip")
	private String g3MediaIp;

	@Column(name = "g3_media_port")
	private Integer g3MediaPort;

	@Column(name = "terminal_types")
	private String terminalTypes;

	@Column(name = "driver_licence_valid")
	private Integer driverLicenceValid;

	@Column(name = "log_term_flag")
	private Integer logTermFlag;

	@Column(name = "log_term_id")
	private String logTermId;

	@Column(name = "bath_delay_time")
	private Integer bathDelayTime;

	@Column(name = "package_size")
	private Integer packageSize;

	@Column(name = "show_car_code")
	private Integer showVehicleCode;

	@Column(name = "group_show_all")
	private Integer groupShowAll;

	@Column(name = "alarm_types")
	private String alarmTypes;

	@Column(name = "gps_status_types")
	private String gpsStatusTypes;

	@Column(name = "goverment_picture_ftp_ip")
	private String govermentPictureFtpIp;

	@Column(name = "goverment_picture_ftp_port")
	private Integer govermentPictureFtpPort;

	@Column(name = "static_date")
	private Date staticDate;
	public static final int MAP_TYPE_GOOGLE = 1;
	public static final int MAP_TYPE_51DITU = 2;
	public static final int MAP_TYPE_MAPABC = 3;
	public static final int MAP_TYPE_MAPBAR = 4;
	public static final int MAP_TYPE_CTTIC = 5;
	public static final int CONNECT_STATUS_NORMAL = 1;
	public static final int CONNECT_STATUS_DIS = 0;
	public static final int MAP_TYPE_GOOGLE_OTHER = 6;

	public Integer getAlaramWaveFlag() {
		if (this.alaramWaveFlag == null)
			return Integer.valueOf(1);
		return this.alaramWaveFlag;
	}

	public void setAlaramWaveFlag(Integer alaramWaveFlag) {
		this.alaramWaveFlag = alaramWaveFlag;
	}

	public boolean isCorrectSmartKey() {
		return (this.smartKey != null) && (!this.smartKey.trim().equals(""));
	}

	public Integer getTaxiFlag() {
		if (this.taxiFlag == null) {
			return Integer.valueOf(0);
		}
		return this.taxiFlag;
	}

	public void setTaxiFlag(Integer taxiFlag) {
		this.taxiFlag = taxiFlag;
	}

	public Integer getPopConfig() {
		if (this.popConfig == null) {
			return Integer.valueOf(0);
		}
		return this.popConfig;
	}

	public void setPopConfig(Integer popConfig) {
		this.popConfig = popConfig;
	}

	public double getInitLat() {
		return this.initLat;
	}

	public void setInitLat(double initLat) {
		this.initLat = initLat;
	}

	public double getInitLng() {
		return this.initLng;
	}

	public void setInitLng(double initLng) {
		this.initLng = initLng;
	}

	public Integer getInitZoomLevel() {
		return this.initZoomLevel;
	}

	public void setInitZoomLevel(Integer initZoomLevel) {
		this.initZoomLevel = initZoomLevel;
	}

	public Integer getGroupShowAll() {
		if (this.groupShowAll == null) {
			return Integer.valueOf(0);
		}
		return this.groupShowAll;
	}

	public void setGroupShowAll(Integer groupShowAll) {
		this.groupShowAll = groupShowAll;
	}

	public String getG3MediaIp() {
		if (this.g3MediaIp == null)
			return "";
		return this.g3MediaIp;
	}

	public void setG3MediaIp(String g3MediaIp) {
		this.g3MediaIp = g3MediaIp;
	}

	public Integer getG3MediaPort() {
		if (this.g3MediaPort == null)
			return Integer.valueOf(30008);
		return this.g3MediaPort;
	}

	public void setG3MediaPort(Integer g3MediaPort) {
		this.g3MediaPort = g3MediaPort;
	}

	public Integer getCarDataDownTime() {
		if (this.carDataDownTime == null)
			return Integer.valueOf(2);
		return this.carDataDownTime;
	}

	public void setCarDataDownTime(Integer carDataDownTime) {
		this.carDataDownTime = carDataDownTime;
	}

	public int getUpdateLatlonInterval() {
		return this.updateLatlonInterval;
	}

	public void setUpdateLatlonInterval(int updateLatlonInterval) {
		this.updateLatlonInterval = updateLatlonInterval;
	}

	public String getSystemTitle() {
		return this.systemTitle;
	}

	public void setSystemTitle(String systemTitle) {
		this.systemTitle = systemTitle;
	}

	public String getGoogleUrl() {
		return this.googleUrl;
	}

	public void setGoogleUrl(String googleUrl) {
		this.googleUrl = googleUrl;
	}

	public String getGoogleVersion() {
		return this.googleVersion;
	}

	public void setGoogleVersion(String googleVersion) {
		this.googleVersion = googleVersion;
	}

	public String getGoogleKey1() {
		return this.googleKey1;
	}

	public void setGoogleKey1(String googleKey1) {
		this.googleKey1 = googleKey1;
	}

	public String getGoogleKey2() {
		return this.googleKey2;
	}

	public void setGoogleKey2(String googleKey2) {
		this.googleKey2 = googleKey2;
	}

	public String getMapabcKey1() {
		return this.mapabcKey1;
	}

	public void setMapabcKey1(String mapabcKey1) {
		this.mapabcKey1 = mapabcKey1;
	}

	public String getMapabcKey2() {
		return this.mapabcKey2;
	}

	public void setMapabcKey2(String mapabcKey2) {
		this.mapabcKey2 = mapabcKey2;
	}

	public String getAuthenticationCode() {
		return this.authenticationCode;
	}

	public void setAuthenticationCode(String authenticationCode) {
		this.authenticationCode = authenticationCode;
	}

	public String getAccessPointName() {
		return this.accessPointName;
	}

	public void setAccessPointName(String accessPointName) {
		this.accessPointName = accessPointName;
	}

	public String getAuthUsername() {
		return this.authUsername;
	}

	public void setAuthUsername(String authUsername) {
		this.authUsername = authUsername;
	}

	public String getAuthPassword() {
		return this.authPassword;
	}

	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}

	public String getUrgencyServerIp() {
		return this.urgencyServerIp;
	}

	public void setUrgencyServerIp(String urgencyServerIp) {
		this.urgencyServerIp = urgencyServerIp;
	}

	public Integer getUrgencyTcpPort() {
		return this.urgencyTcpPort;
	}

	public void setUrgencyTcpPort(Integer urgencyTcpPort) {
		this.urgencyTcpPort = urgencyTcpPort;
	}

	public Integer getUrgencyUdpPort() {
		return this.urgencyUdpPort;
	}

	public void setUrgencyUdpPort(Integer urgencyUdpPort) {
		this.urgencyUdpPort = urgencyUdpPort;
	}

	public Integer getPostQueryInteval() {
		return this.postQueryInteval;
	}

	public void setPostQueryInteval(Integer postQueryInteval) {
		this.postQueryInteval = postQueryInteval;
	}

	public Integer getAlarmRedoTime() {
		return this.alarmRedoTime;
	}

	public void setAlarmRedoTime(Integer alarmRedoTime) {
		this.alarmRedoTime = alarmRedoTime;
	}

	public Integer getAlarmRedoEndTime() {
		return this.alarmRedoEndTime;
	}

	public void setAlarmRedoEndTime(Integer alarmRedoEndTime) {
		this.alarmRedoEndTime = alarmRedoEndTime;
	}

	public String getAlarmSupervisor() {
		return this.alarmSupervisor;
	}

	public void setAlarmSupervisor(String alarmSupervisor) {
		this.alarmSupervisor = alarmSupervisor;
	}

	public String getAlarmSupervisorMail() {
		return this.alarmSupervisorMail;
	}

	public void setAlarmSupervisorMail(String alarmSupervisorMail) {
		this.alarmSupervisorMail = alarmSupervisorMail;
	}

	public String getAlarmSupervisorTel() {
		return this.alarmSupervisorTel;
	}

	public void setAlarmSupervisorTel(String alarmSupervisorTel) {
		this.alarmSupervisorTel = alarmSupervisorTel;
	}

	public Integer getCrossReginFlag() {
		if (this.crossReginFlag == null) {
			return Integer.valueOf(0);
		}
		return this.crossReginFlag;
	}

	public void setCrossReginFlag(Integer crossReginFlag) {
		this.crossReginFlag = crossReginFlag;
	}

	public Integer getPollingOnLineCommputeTime() {
		return this.pollingOnLineCommputeTime;
	}

	public void setPollingOnLineCommputeTime(Integer pollingOnLineCommputeTime) {
		this.pollingOnLineCommputeTime = pollingOnLineCommputeTime;
	}

	public Integer getAutoPostQueryCmdInteval() {
		return this.autoPostQueryCmdInteval;
	}

	public void setAutoPostQueryCmdInteval(Integer autoPostQueryCmdInteval) {
		this.autoPostQueryCmdInteval = autoPostQueryCmdInteval;
	}

	public String getYzInterfaceIp() {
		return this.yzInterfaceIp;
	}

	public void setYzInterfaceIp(String yzInterfaceIp) {
		this.yzInterfaceIp = yzInterfaceIp;
	}

	public Long getYzInterfacePort() {
		return this.yzInterfacePort;
	}

	public void setYzInterfacePort(Long yzInterfacePort) {
		this.yzInterfacePort = yzInterfacePort;
	}

	public String getYzUserId() {
		return this.yzUserId;
	}

	public void setYzUserId(String yzUserId) {
		this.yzUserId = yzUserId;
	}

	public String getYzPassword() {
		return this.yzPassword;
	}

	public void setYzPassword(String yzPassword) {
		this.yzPassword = yzPassword;
	}

	public String getCarControlPassword() {
		return this.carControlPassword;
	}

	public void setCarControlPassword(String carControlPassword) {
		this.carControlPassword = carControlPassword;
	}

	public Integer getSystemFlag() {
		if (this.systemFlag == null) {
			return Integer.valueOf(0);
		}
		return this.systemFlag;
	}

	public void setSystemFlag(Integer systemFlag) {
		this.systemFlag = systemFlag;
	}

	public Integer getMapType() {
		if (this.mapType == null) {
			return Integer.valueOf(1);
		}
		return this.mapType;
	}

	public void setMapType(Integer mapType) {
		this.mapType = mapType;
	}

	public Date getStaticDate() {
		if (this.staticDate == null) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(5, -2);
			return calendar.getTime();
		}
		return this.staticDate;
	}

	public void setStaticDate(Date staticDate) {
		this.staticDate = staticDate;
	}

	public Integer getSuperiorGovStatus() {
		if (this.superiorGovStatus == null) {
			return Integer.valueOf(0);
		}
		return this.superiorGovStatus;
	}

	public void setSuperiorGovStatus(Integer superiorGovStatus) {
		this.superiorGovStatus = superiorGovStatus;
	}

	public Integer getStartGovmentConnect() {
		if (this.startGovmentConnect == null) {
			return Integer.valueOf(0);
		}
		return this.startGovmentConnect;
	}

	public void setStartGovmentConnect(Integer startGovmentConnect) {
		this.startGovmentConnect = startGovmentConnect;
	}

	public Integer getCenterServerStatus() {
		return this.centerServerStatus;
	}

	public void setCenterServerStatus(Integer centerServerStatus) {
		this.centerServerStatus = centerServerStatus;
	}

	public Long getGovmenterCenterId() {
		if (this.govmenterCenterId == null) {
			return Long.valueOf(0L);
		}
		return this.govmenterCenterId;
	}

	public void setGovmenterCenterId(Long govmenterCenterId) {
		this.govmenterCenterId = govmenterCenterId;
	}

	public String getGovernmentIp() {
		return this.governmentIp;
	}

	public void setGovernmentIp(String governmentIp) {
		this.governmentIp = governmentIp;
	}

	public String getGovernmentAccount() {
		return this.governmentAccount;
	}

	public void setGovernmentAccount(String governmentAccount) {
		this.governmentAccount = governmentAccount;
	}

	public String getGovernmentPassword() {
		return this.governmentPassword;
	}

	public void setGovernmentPassword(String governmentPassword) {
		this.governmentPassword = governmentPassword;
	}

	public String getGovSubConnectIp() {
		if (this.govSubConnectIp == null) {
			return "";
		}
		return this.govSubConnectIp;
	}

	public void setGovSubConnectIp(String govSubConnectIp) {
		this.govSubConnectIp = govSubConnectIp;
	}

	public Integer getGovSubConnectPort() {
		if (this.govSubConnectPort == null) {
			return Integer.valueOf(0);
		}
		return this.govSubConnectPort;
	}

	public void setGovSubConnectPort(Integer govSubConnectPort) {
		this.govSubConnectPort = govSubConnectPort;
	}

	public Integer getGovSubConnectStatus() {
		if (this.govSubConnectStatus == null) {
			return Integer.valueOf(0);
		}
		return this.govSubConnectStatus;
	}

	public void setGovSubConnectStatus(Integer govSubConnectStatus) {
		this.govSubConnectStatus = govSubConnectStatus;
	}

	public Long getGovmentM1() {
		if (this.govmentM1 == null)
			return Long.valueOf(0L);
		return this.govmentM1;
	}

	public void setGovmentM1(long govmentM1) {
		this.govmentM1 = Long.valueOf(govmentM1);
	}

	public Long getGovmentIa1() {
		if (this.govmentIa1 == null)
			return Long.valueOf(0L);
		return this.govmentIa1;
	}

	public void setGovmentIa1(Long govmentIa1) {
		this.govmentIa1 = govmentIa1;
	}

	public Long getGovmentIc1() {
		if (this.govmentIc1 == null)
			return Long.valueOf(0L);
		return this.govmentIc1;
	}

	public void setGovmentIc1(Long govmentIc1) {
		this.govmentIc1 = govmentIc1;
	}

	public Boolean getGovmentEncryptFlag() {
		if (this.govmentEncryptFlag == null) {
			return Boolean.valueOf(false);
		}
		return this.govmentEncryptFlag;
	}

	public void setGovmentEncryptFlag(Boolean govmentEncryptFlag) {
		this.govmentEncryptFlag = govmentEncryptFlag;
	}

	public Integer getGovermentPort() {
		if (this.govermentPort == null) {
			return Integer.valueOf(0);
		}
		return this.govermentPort;
	}

	public void setGovermentPort(Integer govermentPort) {
		this.govermentPort = govermentPort;
	}

	public Integer getSystemMode() {
		if (this.systemMode == null)
			return Integer.valueOf(0);
		return this.systemMode;
	}

	public void setSystemMode(Integer systemMode) {
		this.systemMode = systemMode;
	}

	public void setGovmentM1(Long govmentM1) {
		this.govmentM1 = govmentM1;
	}

	public Integer getGpsdataDelayTime() {
		return this.gpsdataDelayTime;
	}

	public void setGpsdataDelayTime(Integer gpsdataDelayTime) {
		this.gpsdataDelayTime = gpsdataDelayTime;
	}

	public Integer getLogoFlag() {
		if (this.logoFlag == null) {
			return Integer.valueOf(0);
		}
		return this.logoFlag;
	}

	public void setLogoFlag(Integer logoFlag) {
		this.logoFlag = logoFlag;
	}

	public Integer getDriverschoolFlag() {
		if (this.driverschoolFlag == null) {
			return Integer.valueOf(0);
		}
		return this.driverschoolFlag;
	}

	public void setDriverschoolFlag(Integer driverschoolFlag) {
		this.driverschoolFlag = driverschoolFlag;
	}

	public Integer getDriverTermType() {
		if (this.driverTermType == null) {
			return Integer.valueOf(30);
		}
		return this.driverTermType;
	}

	public void setDriverTermType(Integer driverTermType) {
		this.driverTermType = driverTermType;
	}

	public Integer getJsDebugFlag() {
		if (this.jsDebugFlag == null) {
			return Integer.valueOf(0);
		}
		return this.jsDebugFlag;
	}

	public void setJsDebugFlag(Integer jsDebugFlag) {
		this.jsDebugFlag = jsDebugFlag;
	}

	public Integer getGovConnectInfo() {
		if (this.govConnectInfo == null) {
			return Integer.valueOf(0);
		}
		return this.govConnectInfo;
	}

	public void setGovConnectInfo(Integer govConnectInfo) {
		this.govConnectInfo = govConnectInfo;
	}

	public String getSmartKey() {
		return this.smartKey;
	}

	public void setSmartKey(String smartKey) {
		this.smartKey = smartKey;
	}

	public String getTerminalTypes() {
		return this.terminalTypes;
	}

	public void setTerminalTypes(String terminalTypes) {
		this.terminalTypes = terminalTypes;
	}

	public Integer getDriverLicenceValid() {
		return this.driverLicenceValid;
	}

	public void setDriverLicenceValid(Integer driverLicenceValid) {
		this.driverLicenceValid = driverLicenceValid;
	}

	public Integer getLogTermFlag() {
		if (this.logTermFlag == null) {
			return Integer.valueOf(0);
		}
		return this.logTermFlag;
	}

	public void setLogTermFlag(Integer logTermFlag) {
		this.logTermFlag = logTermFlag;
	}

	public String getLogTermId() {
		return this.logTermId;
	}

	public void setLogTermId(String logTermId) {
		this.logTermId = logTermId;
	}

	public Integer getBathDelayTime() {
		return this.bathDelayTime;
	}

	public void setBathDelayTime(Integer bathDelayTime) {
		this.bathDelayTime = bathDelayTime;
	}

	public Integer getShowVehicleCode() {
		return Integer.valueOf(this.showVehicleCode == null ? 0
				: this.showVehicleCode.intValue());
	}

	public void setShowVehicleCode(Integer showVehicleCode) {
		this.showVehicleCode = showVehicleCode;
	}

	public Integer getPackageSize() {
		if (this.packageSize == null) {
			return Integer.valueOf(512);
		}
		return this.packageSize;
	}

	public void setPackageSize(Integer packageSize) {
		this.packageSize = packageSize;
	}

	public String getAlarmTypes() {
		return this.alarmTypes;
	}

	public void setAlarmTypes(String alarmTypes) {
		this.alarmTypes = alarmTypes;
	}

	public String getGpsStatusTypes() {
		return this.gpsStatusTypes;
	}

	public void setGpsStatusTypes(String gpsStatusTypes) {
		this.gpsStatusTypes = gpsStatusTypes;
	}

	public String getGovermentPictureFtpIp() {
		return this.govermentPictureFtpIp;
	}

	public void setGovermentPictureFtpIp(String govermentPictureFtpIp) {
		this.govermentPictureFtpIp = govermentPictureFtpIp;
	}

	public Integer getGovermentPictureFtpPort() {
		if (this.govermentPictureFtpPort == null) {
			return Integer.valueOf(21);
		}
		return this.govermentPictureFtpPort;
	}

	public void setGovermentPictureFtpPort(Integer govermentPictureFtpPort) {
		this.govermentPictureFtpPort = govermentPictureFtpPort;
	}
}

/*
 * Location: E:\goss_web\goss_service\goss_service.jar Qualified Name:
 * com.yuweitek.goss.model.SystemConfig JD-Core Version: 0.6.0
 */