package com.ltmonitor.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@org.hibernate.annotations.Proxy(lazy = false)
@Table(name = "UserInfo", uniqueConstraints = { @javax.persistence.UniqueConstraint(columnNames = { "loginName" }) })
public class UserInfo extends TenantEntity implements Serializable {
	private static final long serialVersionUID = -4356713910438316680L;
	private int entityId;
	
	public static String STATE_SUSPEND = "suspend"; //暂停该用户
	public static String STATE_NORMAL = "normal"; //可用状态
	//中文姓名
	private String name;
	//唯一登录ID
	private String loginName = "";
	private String password = "";
	private String userState ;
	//地图中心
	private double mapCenterLat;
	private double mapCenterLng;
	private int mapLevel;
	//地图类型 
	private int mapType = 0;
	private int userFlag = 1;
	private String ip;
	public static final int USER_TYPE_ADMIN = 0;
	public static final int USER_TYPE_CARUSER = 1;
	private int userType = 0;
	//用户分配的车组部门
	private Set<Department> departments = new HashSet();
	//用户分配的角色
	private Set<Role> roles = new HashSet();
	public static final int USER_FLAG_SUPER_ADMIN = 2;
	public static final int USER_FLAG_WEBGIS = 1;
	private Date updateTime = new Date();
	@Transient
	private String orgNo;

	public UserInfo() {
		userState = STATE_NORMAL;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "userId", unique = true, nullable = false)
	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int id) {
		this.entityId = id;
	}

	@Column(name = "loginName", unique = true, nullable = false, length = 50)
	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String name) {
		this.loginName = name;
	}

	@Column(name = "password", nullable = false, length = 16)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	@Column(name = "userState")
	public String getUserState() {
		return this.userState;
	}

	public void setUserState(String useState) {
		this.userState = useState;
	}

	@Transient
	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "UserDepartment", joinColumns = { @javax.persistence.JoinColumn(name = "userId") }, inverseJoinColumns = { @javax.persistence.JoinColumn(name = "depId") })
	public Set<Department> getDepartments() {
		return this.departments;
	}

	public void setDepartments(Set<Department> departments) {
		this.departments = departments;
	}

	


	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "UserRole", joinColumns = { @javax.persistence.JoinColumn(name = "userId") }, inverseJoinColumns = { @javax.persistence.JoinColumn(name = "roleId") })
	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Transient
	public Role getRole() {
		Iterator i$ = this.roles.iterator();
		if (i$.hasNext()) {
			Role role = (Role) i$.next();
			return role;
		}
		return null;
	}

	@Transient
	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Transient
	public int getUserType() {
		return this.userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public double getMapCenterLat() {
		return this.mapCenterLat;
	}

	public void setMapCenterLat(double mapCenterLat) {
		this.mapCenterLat = mapCenterLat;
	}


	public int getMapLevel() {
		return this.mapLevel;
	}

	public void setMapLevel(int mapLevel) {
		this.mapLevel = mapLevel;
	}

	public int getMapType() {
		return this.mapType;
	}

	public void setMapType(int mapType) {
		this.mapType = mapType;
	}

	public int getUserFlag() {
		return this.userFlag;
	}

	public void setUserFlag(int userFlag) {
		this.userFlag = userFlag;
	}


	@Transient
	public String getOrgNo() {
		return this.orgNo;
	}

	@Transient
	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMapCenterLng() {
		return mapCenterLng;
	}

	public void setMapCenterLng(double mapCenterLng) {
		this.mapCenterLng = mapCenterLng;
	}
}
