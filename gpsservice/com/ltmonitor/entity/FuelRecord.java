﻿package com.ltmonitor.entity;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//油量记录，10分钟记录一次，
public class FuelRecord
{
	public FuelRecord()
	{
		setSendTime(new java.util.Date());
	}

	private int  entityId;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "funcId", unique = true, nullable = false)
	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int id) {
		this.entityId = id;
	}
	//数据所来自的命令包的ID号
	private int commandId;
	public final int getCommandId()
	{
		return commandId;
	}
	public final void setCommandId(int value)
	{
		commandId = value;
	}
	//车终端卡号
	private String privateVehicleId;
	public final String getVehicleId()
	{
		return privateVehicleId;
	}
	public final void setVehicleId(String value)
	{
		privateVehicleId = value;
	}

	//车牌号
	private String privatePlateNo;
	public final String getPlateNo()
	{
		return privatePlateNo;
	}
	public final void setPlateNo(String value)
	{
		privatePlateNo = value;
	}
	//订单号
	private String orderId;
	public final String getOrderId()
	{
		return orderId;
	}
	public final void setOrderId(String value)
	{
		orderId = value;
	}
	//发送时间
	private java.util.Date privateSendTime = new java.util.Date(0);
	public final java.util.Date getSendTime()
	{
		return privateSendTime;
	}
	public final void setSendTime(java.util.Date value)
	{
		privateSendTime = value;
	}
	//经度
	private double longitude;
	public final double getLongitude()
	{
		return longitude;
	}
	public final void setLongitude(double value)
	{
		longitude = value;
	}
	//纬度
	private double latitude;
	public final double getLatitude()
	{
		return latitude;
	}
	public final void setLatitude(double value)
	{
		latitude = value;
	}
	//速度
	private double privateVelocity;
	public final double getVelocity()
	{
		return privateVelocity;
	}
	public final void setVelocity(double value)
	{
		privateVelocity = value;
	}
	//对经纬度的地理位置解析
	private String location;
	public final String getLocation()
	{
		return location;
	}
	public final void setLocation(String value)
	{
		location = value;
	}
	//方向
	private int privateDirection;
	public final int getDirection()
	{
		return privateDirection;
	}
	public final void setDirection(int value)
	{
		privateDirection = value;
	}
	//状态
	private String privateStatus;
	public final String getStatus()
	{
		return privateStatus;
	}
	public final void setStatus(String value)
	{
		privateStatus = value;
	}
	//报警位状态
	private String alarmState;
	public final String getAlarmState()
	{
		return alarmState;
	}
	public final void setAlarmState(String value)
	{
		alarmState = value;
	}
	//里程
	private double privateMileage;
	public final double getMileage()
	{
		return privateMileage;
	}
	public final void setMileage(double value)
	{
		privateMileage = value;
	}
	//油量
	private double gas;
	public final double getGas()
	{
		return gas;
	}
	public final void setGas(double value)
	{
		gas = value;
	}

	//行驶记录仪速度
	private double privateRecordVelocity;
	public final double getRecordVelocity()
	{
		return privateRecordVelocity;
	}
	public final void setRecordVelocity(double value)
	{
		privateRecordVelocity = value;
	}
}