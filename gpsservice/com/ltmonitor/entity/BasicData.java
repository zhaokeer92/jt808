package com.ltmonitor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * 基础数据定义.用于定义配置系统中所有的公用基础数据。
 * @author zhengxiny
 *
 */

@Entity
@Table(name="basicData")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class BasicData extends TenantEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "baseId", unique = true, nullable = false)
	private int entityId;
	
	private String name;

	private String code;

	private String parent;
	//扩展定义数据
	private String meta;
	
	
	//用于排序的序号
	private int sn;
	
	public BasicData(){		
	}
	
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public int getSn() {
		return sn;
	}

	public void setSn(int sn) {
		this.sn = sn;
	}
}
