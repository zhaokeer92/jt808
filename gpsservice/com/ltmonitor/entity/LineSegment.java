﻿package com.ltmonitor.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

//**********************************************************************
// 线路中的每个拐点                                                              
//**********************************************************************

@Entity
@Table(name="LineSegment")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class LineSegment extends TenantEntity implements Serializable
{
	public static String InDriver = "进区域报警给驾驶员";
	public static String InPlatform = "进区域报警给平台";
	public static String OutDriver = "出区域报警给驾驶员";
	public static String OutPlatform = "出区域报警给平台 ";

	public LineSegment()
	{
		setCreateDate(new java.util.Date());
		this.lineWidth = 50;
		this.limitSpeed = true;
		this.maxSpeed = 80;
		this.overSpeedTime = 10;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "segId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	

	private String name;
	public final String getName()
	{
		return name;
	}
	public final void setName(String value)
	{
		name = value;
	}

	private int enclosureId;
	public final int getEnclosureId()
	{
		return enclosureId;
	}
	public final void setEnclosureId(int value)
	{
		enclosureId = value;
	}

	//拐点ID
	private int pointId;
	public final int getPointId()
	{
		return pointId;
	}
	public final void setPointId(int value)
	{
		pointId = value;
	}
	//经度
	private double latitude1;
	public final double getLatitude1()
	{
		return latitude1;
	}
	public final void setLatitude1(double value)
	{
		latitude1 = value;
	}
	//纬度
	private double longitude1;
	public final double getLongitude1()
	{
		return longitude1;
	}
	public final void setLongitude1(double value)
	{
		longitude1 = value;
	}
	

	private double latitude2;
	private double longitude2;
	//是否包站点
	private boolean station;
	public final boolean isStation()
	{
		return station;
	}
	public final void setStation(boolean value)
	{
		station = value;
	}

	//路段宽度
	private int lineWidth;
	public final int getLineWidth()
	{
		return lineWidth;
	}
	public final void setLineWidth(int value)
	{
		lineWidth = value;
	}
	//路段属性,各个选项使用;分割
	private String alarmType;
	public final String getAlarmType()
	{
		return alarmType;
	}
	public final void setAlarmType(String value)
	{
		alarmType = value;
	}
	//路段行驶时间限制
	private int maxTimeLimit;
	public final int getMaxTimeLimit()
	{
		return maxTimeLimit;
	}
	public final void setMaxTimeLimit(int value)
	{
		maxTimeLimit = value;
	}

	private int minTimeLimit;
	public final int getMinTimeLimit()
	{
		return minTimeLimit;
	}
	public final void setMinTimeLimit(int value)
	{
		minTimeLimit = value;
	}
	//超速限制
	private int maxSpeed;
	public final int getMaxSpeed()
	{
		return maxSpeed;
	}
	public final void setMaxSpeed(int value)
	{
		maxSpeed = value;
	}
	//超速持续时间
	private int overSpeedTime;
	public final int getOverSpeedTime()
	{
		return overSpeedTime;
	}
	public final void setOverSpeedTime(int value)
	{
		overSpeedTime = value;
	}

	private boolean byTime;
	public final boolean getByTime()
	{
		return byTime;
	}
	public final void setByTime(boolean value)
	{
		byTime = value;
	}

	private boolean limitSpeed;
	public final boolean getLimitSpeed()
	{
		return limitSpeed;
	}
	public final void setLimitSpeed(boolean value)
	{
		limitSpeed = value;
	}

	//创建区域属性
	public final byte CreateAreaAttr()
	{
		String byteStr = "";
		byteStr += (byte)(getByTime() ? 1 : 0); //1：根据时间
		byteStr += (byte)(getLimitSpeed() ? 1 : 0); //限速
		byteStr += 0;
		byteStr += 0;

		while(byteStr.length() < 7)
		{
			byteStr += '0';
		}
		byteStr += (byte)(isStation() ? 1 : 0); //1：是否有报站点

		byte t = Byte.parseByte(byteStr, 2);
		return t;
	}
	public double getLatitude2() {
		return latitude2;
	}
	public void setLatitude2(double latitude2) {
		this.latitude2 = latitude2;
	}
	public double getLongitude2() {
		return longitude2;
	}
	public void setLongitude2(double longitude2) {
		this.longitude2 = longitude2;
	}
}